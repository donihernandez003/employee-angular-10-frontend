import { Component, OnInit } from '@angular/core';
import {EmployeeService} from '../../services/employee.service';
import { NgForm } from '@angular/forms';
import {Employee} from '../../models/employee';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  constructor(public employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.getEmployees();
  }

  resetForm(form: NgForm): void {
    form.reset();
  }

  getEmployees(): any {
    this.employeeService.getEmployees().subscribe(
      res => {
        this.employeeService.employees = res;
      },
      err => console.error(err)
    );
  }

  addEmployee(form: NgForm): void {
    if (form.value._id) {
      this.employeeService.updateEmployee(form.value)
        .subscribe(
          res => {
            this.getEmployees();
            form.reset();
          },
          err => console.error(err)
        );
    } else {
      this.employeeService.createEmployee(form.value)
        .subscribe(
          res => {
            this.getEmployees();
            form.reset();
          },
          err => console.error(err)
        );
    }
  }

  editEmployee(employee: Employee): void {
    this.employeeService.selectedEmployee = employee;
  }

  deleteEmployee(id: string): void {
    if (confirm('Are you sure you want to delete it?')) {
      this.employeeService.deleteEmployee(id)
        .subscribe(
          res => {
            this.getEmployees();
          },
          err => console.error(err)
        );
    }
  }
}
