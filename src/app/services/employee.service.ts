import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../models/employee';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  URL_API = 'http://localhost:5000/api/employees';

  selectedEmployee: Employee = {
    name: '',
    position: '',
    office: '',
    salary: 0
  };
  employees: Employee[];

  constructor(private http: HttpClient) { }

  getEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.URL_API);
  }

  createEmployee(employee: Employee): Observable<Employee> {
    return this.http.post<Employee>(this.URL_API, employee);
  }

  updateEmployee(employee: Employee): Observable<any> {
      return this.http.put(`${this.URL_API}/${employee._id}`, employee);
  }

  deleteEmployee(id: string): Observable<any> {
    return this.http.delete(`${this.URL_API}/${id}`);
  }
}
